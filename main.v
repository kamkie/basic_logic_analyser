`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:38:23 01/15/2014 
// Design Name: 
// Module Name:    main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main(
    input clk_i,
    input rst_i,
	 input [7:0] sw,
    input [7:0] JA,
    input [7:0] JB,
    input [7:0] JC,
	 output reg [7:0] Led
    );
	 
	 //reg [7:0] sw;
	 //reg [7:0] JA;
	 //reg [7:0] JB;
	 //reg [7:0] JC;
	 wire [35:0] CONTROL0;
	 reg [23:0] TRIG0;
	 
	 icon icon1 (
    .CONTROL0(CONTROL0) // INOUT BUS [35:0]
);

ila ila0 (
    .CONTROL(CONTROL0), // INOUT BUS [35:0]
    .CLK(clk_i), // IN
    .TRIG0(TRIG0) // IN BUS [23:0]
);
	 
	 always @(posedge clk_i) 
	 begin: diody
	 TRIG0 <= {JA,JB,JC};
	 if (sw==0)
	 begin
		Led<=JA;
	 end
	 if (sw==1)
	 begin
		Led<=JB;
	 end 
	 if (sw==2)
	 begin
		Led<=JC;
	 end 	 
	 end


endmodule
